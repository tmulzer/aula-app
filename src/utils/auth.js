function getLoginData (url) {
  return {
    url: url + 'rpc/login',
    method: 'POST',
    redirect: '/',
    fetchUser: true
  }
}

function getFetchData (url) {
  return {
    url: url + 'rpc/me',
    enabled: true
  }
}

export { getLoginData, getFetchData }
