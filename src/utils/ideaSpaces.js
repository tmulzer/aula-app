import api from '../api'

export async function getIdeaSpaces (schoolId, translator) {
  return api.ideaSpace.getIdeaSpaces(schoolId).then((res) => {
    // Return value is transformed so that it is suitable for
    // vuetify's select component
    let ideaSpaces = []
    ideaSpaces = res.data.map(sp => ({
      text: sp.title,
      value: sp.id
    }))
    // Add a no-space-assigned option here
    ideaSpaces.push({
      text: translator('$vuetify.AdminUsers.formNoSpaceSelected'),
      value: null
    })
    return ideaSpaces
  })
}
