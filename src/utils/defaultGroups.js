import state from '../store'

export function getDefaultGroups (translator) {
  const defaultGroups = [
    {value: 'student', text: translator('$vuetify.roles.student')},
    {value: 'class_guest', text: translator('$vuetify.roles.class_guest')},
    {value: 'school_guest', text: translator('$vuetify.roles.school_guest')},
    {value: 'moderator', text: translator('$vuetify.roles.moderator')},
    {value: 'principal', text: translator('$vuetify.roles.principal')},
    {value: 'school_admin', text: translator('$vuetify.roles.school_admin')},
    {value: 'admin', text: translator('$vuetify.roles.admin')}
  ]
  const currentUserRoles = state.getters.user.profile.roles.map(function (roleArray) {
    return roleArray[0]
  })
  if (currentUserRoles.indexOf('admin') < 0) {
    const adminIndex = defaultGroups.indexOf(defaultGroups.find(function (group) {
      return group.value === 'admin'
    }))
    defaultGroups.splice(adminIndex, 1)
  }
  return defaultGroups
}
