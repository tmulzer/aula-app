import Store from '../store'

function hasPermission (permission) {
  if (isUserAdmin() === true) {
    return true
  }

  let rolePermissions = Store.getters.permissions
  if (rolePermissions === null) return false

  // eslint-disable-next-line no-unused-vars
  for (let [role, permissions] of Object.entries(rolePermissions)) {
    if (permission in permissions) {
      return true
    }
  }
  return false
}

function hasPermissionInIdeaSpace (permission, ideaSpace) {
  if (isUserAdmin() === true) {
    return true
  }

  let ideaSpaceId
  // if idea space id is not present set it to 0 (main room)
  if (!ideaSpace || !ideaSpace.id) {
    ideaSpaceId = 0
  } else {
    ideaSpaceId = ideaSpace.id
  }

  let rolePermissions = Store.getters.permissions
  let roles = Store.getters.roles

  if (rolePermissions === null) return false
  if (roles === null) return false

  for (let [role, permissions] of Object.entries(rolePermissions)) {
    if (permission in permissions) {
      // if permission has validity set to all (school wide) then return true
      if (permissions[permission].some(validity => validity === Validity.all)) return true

      // if permission has validity set to main and idea space is main (id: 0) return true
      if (permissions[permission].some(
        validity => validity === Validity.school || validity === Validity.school_and_idea_space) && ideaSpaceId === 0) {
        return true
      }

      // otherwise, search for role with permission and correct ideaSpace
      let roleIdeaSpaces = roles[role]
      if (roleIdeaSpaces) {
        if (roleIdeaSpaces.some(sp => sp === ideaSpaceId)) {
          return true
        }
      }
    }
  }
  console.log('no permission')
  // permission key not found in user roles return false
  return false
}

function hasValidity (rolePermissions, validity) {
  let foundValidity = false
  Object.keys(rolePermissions).every(key => {
    let permissions = rolePermissions[key]
    Object.keys(rolePermissions[key]).every(permission => {
      if (permissions[permission].includes(validity)) {
        foundValidity = true
      }
      return !foundValidity
    })
    return !foundValidity
  })
  return foundValidity
}

function isUserSuperAdmin () {
  let isAdmin = Store.getters.isAdmin

  return isAdmin === true
}

function isUserAdmin () {
  let isAdmin = Store.getters.isAdmin
  let isSchoolAdmin = Store.getters.isSchoolAdmin

  return isAdmin === true || isSchoolAdmin === true
}

const Validity = {
  all: 'all',
  school: 'school',
  idea_space: 'idea_space',
  school_and_idea_space: 'school_and_idea_space'
}

const Permissions = {
  idea: {
    name: 'idea',
    items: {
      createIdea: 'create_idea',
      deleteIdea: 'delete_idea',
      supportIdea: 'support_idea',
      editIdea: 'edit_idea',
      approveIdea: 'approve_idea',
      voteIdea: 'vote_idea',
      markIdeaWinner: 'mark_idea_winner'
    }
  },
  comment: {
    name: 'comment',
    items: {
      createComment: 'create_comment',
      deleteComment: 'delete_comment',
      supportComment: 'support_comment',
      editComment: 'edit_comment'
    }
  },
  topic: {
    name: 'topic',
    items: {
      createTopic: 'create_topic',
      editTopic: 'edit_topic',
      deleteTopic: 'delete_topic',
      addTopicIdea: 'add_topic_idea',
      changeTopicPhase: 'change_topic_phase'
    }
  },
  user: {
    name: 'user',
    items: {
      createUser: 'create_user',
      deleteUser: 'delete_user',
      editUser: 'edit_user'
    }
  },
  role: {
    name: 'role',
    items: {
      createRole: 'create_role',
      deleteRole: 'delete_role',
      editRole: 'edit_role'
    }
  },
  idea_space: {
    name: 'idea_space',
    items: {
      createRoom: 'create_room',
      deleteRoom: 'delete_room',
      editRoom: 'edit_room'
    }
  },
  category: {
    name: 'category',
    items: {
      deleteCategory: 'delete_category',
      createCategory: 'create_category',
      editCategory: 'edit_category'
    }
  },
  admin: {
    name: 'admin',
    items: {seeAdmin: 'see_admin'}
  },
  pages: {
    name: 'pages',
    items: {
      createPage: 'create_page',
      deletePage: 'delete_page',
      editPage: 'edit_page'
    }
  }
}

export default {
  permissions: Permissions,
  validity: Validity,
  hasPermission,
  hasPermissionInIdeaSpace,
  hasValidity,
  isUserAdmin,
  isUserSuperAdmin
}
