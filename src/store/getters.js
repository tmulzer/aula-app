/* eslint-disable no-undef */
const getters = {
  selected_school: state => state.school.selectedSchool,
  school_id: state => state.user.profile.school_id,
  userId: state => state.user.profile.id,
  user: state => state.user,
  locale: state => state.user.locale,
  activeServer: state => state.server.useCustom ? state.server.custom : process.env.BASE_API,
  defaultServer: state => process.env.BASE_API,
  customServer: state => state.server.custom,
  useCustomServer: state => state.server.useCustom,
  schoolName: state => state.school.name,
  schoolConfig: state => state.school.schoolConfig,
  phases: state => state.school.phases,
  phaseOrder: state => state.school.phaseOrder,
  isLoading: state => state.ui.loading,
  firebase_id: state => state.notification.token,
  firebaseOptions: state => state.notification.options,
  permissions: state => state.user.profile.permissions,
  roles: state => state.user.profile.roles,
  hasAllValidity: state => state.user.profile.hasAllValidity,
  isRoleSwitchActive: state => state.user.role_switcher_active,
  isAdmin: state => state.user.profile.is_admin,
  isSchoolAdmin: state => state.user.profile.is_school_admin,
  translationLanguage: state => state.translation.language,
  translateServer: state => process.env.DEEPL_API,
  supportedLanguages: state => state.translation.supportedLanguages
}
export default getters
