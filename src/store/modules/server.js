/* eslint-disable no-undef */
const server = {
  state: {
    custom: '',
    useCustom: false
  },

  mutations: {
    SET_SERVER: (state, currentSever) => {
      state.custom = currentSever
    },
    TOGGLE_SERVER: (state, value) => {
      state.useCustom = value
    }
  }
}

export default server
