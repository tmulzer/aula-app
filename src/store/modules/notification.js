/* eslint-disable no-undef */
const notification = {
  state: {
    token: '',
    options: {
      'newSchoolIdea': false, // if a new wild idea was posted in school
      'newClassIdea': false, // if a new wild idea was posted in my class
      'newSugestionYourIdea': true, // if there is a new suggestion for improving your idea
      'ideaPhaseChange': true, // if your idea is in a new phase
      'schoolTopicPhaseChange': false, // if a topic in your school enters a new phase
      'classTopicPhaseChange': false, // if a topic in your class enters a new phase
      'receiveDelegatedVote': true, // if someone delegates his voice to you
      'ideaQuorumPassed': true, // if your wild idea passes the quorum
      'votedForYou': true, // if someone voted with your vote
      'votingExpiring': false, // if a vote is about to expire
      'subjectReport': false, // if a subject is reported
      'voteReceived': true // if your idea won a vote
    }
  },

  mutations: {
    SET_TOKEN: (state, token) => {
      state.token = token
    },
    SET_NOTIFICATION_OPTIONS: (state, options) => {
      state.options = options
    }
  }
}

export default notification
