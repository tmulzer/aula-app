const translation = {
  state: {
    language: 'default',
    supportedLanguages: [
      { value: 'BG', text: 'Bulgarian' },
      { value: 'ZH', text: 'Chinese' },
      { value: 'CS', text: 'Czech' },
      { value: 'DA', text: 'Danish' },
      { value: 'NL', text: 'Dutch' },
      { value: 'EN', text: 'English' },
      { value: 'ET', text: 'Estonian' },
      { value: 'FI', text: 'Finnish' },
      { value: 'FR', text: 'French' },
      { value: 'DE', text: 'German' },
      { value: 'EL', text: 'Greek' },
      { value: 'HU', text: 'Hungarian' },
      { value: 'IT', text: 'Italian' },
      { value: 'JA', text: 'Japanese' },
      { value: 'LT', text: 'Lithuanian' },
      { value: 'LV', text: 'Latvian' },
      { value: 'PL', text: 'Polish' },
      { value: 'PT', text: 'Portuguese' },
      { value: 'RO', text: 'Romanian' },
      { value: 'RU', text: 'Russian' },
      { value: 'SK', text: 'Slovak' },
      { value: 'SL', text: 'Slovenian' },
      { value: 'ES', text: 'Spanish' },
      { value: 'SV', text: 'Swedish' }
    ]
  },

  mutations: {
    SET_TRANSLATION_LANGUAGE: (state, newLanguage) => {
      state.language = newLanguage
    }
  }
}

export default translation
