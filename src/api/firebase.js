import router from '@/router'
import store from '@/store'

function setupFirebase () {
  window.FirebasePlugin.hasPermission(function (hasPermission) {
    if (!hasPermission) {
      window.FirebasePlugin.grantPermission(function (hasPermission) {
        console.log('Permission was ' + (hasPermission ? 'granted' : 'denied'))
      })
    }
  })

  window.FirebasePlugin.getToken(function (token) {
    store.commit('SET_TOKEN', token)
  }, function (error) {
    console.error(error)
  })

  window.FirebasePlugin.onTokenRefresh(function (token) {
    store.commit('SET_TOKEN', token)
  }, function (error) {
    console.error(error)
  })

  window.FirebasePlugin.onMessageReceived(function (message) {
    try {
      if (message.tap === 'background') {
        router.push({ path: message.url })
      } else {
        var event = new CustomEvent('messageReceived', {'detail': message})
        document.dispatchEvent(event)
      }
    } catch (e) {
      console.log('Exception in onMessageReceived callback: ' + e.message)
    }
  }, function (error) {
    console.log('Failed receiving FirebasePlugin message', error)
  })
}

export default {
  setupFirebase
}
