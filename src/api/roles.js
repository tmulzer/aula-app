import service from '@/api/service'

export function getRoles (schoolId) {
  const params = {
    school_id: `eq.${schoolId}`
  }
  return service.get('/roles', {params})
}

function updateRole (roleId, data) {
  const params = {
    id: `eq.${roleId}`
  }
  return service.patch('/roles', data, {params})
}

function createRole (roleName, schoolId) {
  const body = {
    name: roleName,
    school_id: schoolId
  }
  return service.post('/roles', body)
}

function deleteRole (roleId) {
  const params = {
    id: `eq.${roleId}`
  }
  return service.delete('/roles', {params})
}

export default {
  getRoles,
  updateRole,
  createRole,
  deleteRole
}
