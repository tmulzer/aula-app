import axios from 'axios'
import store from '@/store'

/* eslint-disable no-undef */
const service = axios.create({
  baseURL: store.getters.activeServer,
  timeout: 15000 // request timeout
})

service.interceptors.request.use(config => {
  store.commit('SET_LOADING', true)
  return config
})

service.interceptors.response.use(
  response => {
    store.commit('SET_LOADING', false)
    return response
  },
  error => {
    store.commit('SET_LOADING', false)
    return error
  }
)

export default service
