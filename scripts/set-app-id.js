var fs = require('fs')
var path = require('path')

let appid = 'de.aula'

console.info('Optionally pass a single <appid> (e.g. de.aula.app) command line argument')

if (process.argv.length === 3) {
    appid = (process.argv[2])
}
try {
    const file = path.join(__dirname, '..', 'config.xml')
    fs.readFile(file, 'utf8', function (err, data) {
        if (err) {
            return console.log(err)
        }
        var result = data.replace(/id="de.aula"/g, `id="${appid}"`)

        fs.writeFile(file, result, 'utf8', function (err) {
            if (err) return console.log(err)

            console.info('using app id ', appid)
        })
    })
} catch (err) {
    console.error(err)
}
